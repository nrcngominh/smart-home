const mqtt = require("mqtt");
const app = require("express")();

// Utils
const rand = (max) => {
  return Math.floor(Math.random() * max);
};

// Device status
const status = {
  led: "0", // 0 - OFF, 1 - RED, 2 - BLUE
  door: "0", // 0 - OFF, 1 - ON
  gas: "0", // 0 - SAFE, 1 - DANGER
  speaker: "0", // 0 - 123
};

// Device topics
const TOPICS = {
  LED: process.env.PREFIX1 + "bk-iot-led",
  MAGNETIC: process.env.PREFIX1 + "bk-iot-magnetic",
  DHT11: process.env.PREFIX1 + "bk-iot-temp-humid",
  SPEAKER: process.env.PREFIX1 + "bk-iot-speaker",
  GAS: process.env.PREFIX2 + "bk-iot-gas",
};

// Connect to broker
const url = `mqtt://${process.env.BROKER_HOST1}`;
const url2 = `mqtt://${process.env.BROKER_HOST2}`;
const client = mqtt.connect(url);
const client2 = mqtt.connect(url2);

// REST API
app.get("/dashboard", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

// Socket.IO
const server = require("http").Server(app);
const io = require("socket.io")(server);

server.listen(3000, () => {
  console.log("Server is running");
});

// MQTT
client.on("connect", () => {
  // DHT11
  setInterval(() => {
    const topic = TOPICS.DHT11;
    const temp = 30 + rand(10);
    const humid = 60 + rand(10);
    const data = {
      id: "7",
      name: "TEMP_HUMID",
      data: `${temp}-${humid}`,
      unit: "*C-%",
    };

    client.publish(topic, JSON.stringify(data));
    console.log("SEND: ", { topic, data });
  }, 30000);

  io.on("connection", (socket) => {
    console.log("Client connected");

    // Magnetic
    socket.emit("MAGNETIC", status.door);
    socket.on("MAGNETIC", (value) => {
      status.door = value;
      const topic = TOPICS.MAGNETIC;
      const data = {
        id: "8",
        name: "MAGNETIC",
        data: value,
        unit: "",
      };
      client.publish(topic, JSON.stringify(data));
      console.log("SEND: ", { topic, data });
    });

    // LED
    socket.emit("LED", status.led);
    socket.on("LED", (data) => {
      status.led = data;
    });

    client.subscribe(TOPICS.LED, function (err) {
      if (err) {
        console.error(err);
      }
    });

    // SPEAKER
    socket.emit("SPEAKER", status.speaker);
    socket.on("SPEAKER", (value) => {
      status.speaker = value;
      const topic = TOPICS.GAS;
      const data = {
        id: "3",
        name: "SPEAKER",
        data: status.speaker.toString(),
        unit: "",
      };
      client.publish(topic, JSON.stringify(data));
      console.log("SEND: ", { topic, data });
    });
    client.subscribe(TOPICS.SPEAKER, function (err) {
      if (err) {
        console.error(err);
      }
    });

    client.on("message", (topicByte, messageByte) => {
      const topic = topicByte.toString();
      const wrapper = JSON.parse(messageByte.toString());
      const data = wrapper.data;

      // Handle message
      if (topic === TOPICS.LED) {
        status.led = data;
        socket.emit("LED", status.led);
      } else if (topic === TOPICS.SPEAKER) {
        status.speaker = data;
        socket.emit("SPEAKER", status.speaker);
      }
    });
  });
});

client2.on("connect", () => {
  console.log("Client 2 connected");
  io.on("connection", (socket) => {
    console.log("Client connected");

    // Gas
    socket.emit("GAS", status.gas);
    socket.on("GAS", (value) => {
      status.gas = value;
      const topic = TOPICS.GAS;
      const data = {
        id: "23",
        name: "GAS",
        data: status.gas,
        unit: "",
      };
      console.log("Send GAS request");
      client2.publish(topic, JSON.stringify(data));
      console.log("SEND: ", { topic, data });
    });

    client2.on("message", (topicByte, messageByte) => {
      const topic = topicByte.toString();
      const wrapper = JSON.parse(messageByte.toString());
      const data = wrapper.data;
    });
  });
});

client.on("message", (topicByte, messageByte) => {
  const topic = topicByte.toString();
  const wrapper = JSON.parse(messageByte.toString());
  console.log("RECV: ", { topic, wrapper });
});

client2.on("message", (topicByte, messageByte) => {
  const topic = topicByte.toString();
  const wrapper = JSON.parse(messageByte.toString());
  console.log("RECV: ", { topic, wrapper });
});
