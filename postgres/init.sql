-- TODO: PHONG


CREATE TABLE accounts (
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  password VARCHAR(255) NOT NULL
);

INSERT INTO accounts(username, password) VALUES('abcdef', '123456'),
												('admin', '123');

CREATE TABLE rooms (
	roomid SERIAL PRIMARY KEY,
	roomname VARCHAR(255) UNIQUE NOT NULL
);
INSERT INTO rooms(roomname) VALUES( 'Phong khach'),
									( 'Phong ngu'),
									('Phong khach so 2'),
									('Phong bep');

CREATE TABLE leds(
	ledid SERIAL PRIMARY KEY,
	roomid INT,
	CONSTRAINT fk_roomid
      FOREIGN KEY(roomid)
	  REFERENCES rooms(roomid)
	  ON DELETE SET NULL
);

INSERT INTO leds(roomid) VALUES( 1),(2), (3),(4);

-- temperature && humid-------------------------------------------
-- sensor
CREATE TABLE temp_humid_sensors(
	thid SERIAL PRIMARY KEY,
	roomid INT,
	CONSTRAINT fk_roomled
      FOREIGN KEY(roomid)
	  REFERENCES rooms(roomid)
	  ON DELETE SET NULL
);

INSERT INTO temp_humid_sensors(roomid)
		VALUES(1),(2), (3),(4);

-- record
CREATE TABLE temp_records(
	tempid SERIAL PRIMARY KEY,
	temperature FLOAT,
	roomid INT,
	datetime TIMESTAMP,
	CONSTRAINT fk_roomled
      FOREIGN KEY(roomid)
	  REFERENCES rooms(roomid)
	  ON DELETE SET NULL
);

INSERT INTO temp_records(temperature, roomid, datetime)
		VALUES(30.0,1,'2021-05-01 19:10:25'),
			(36.0,1,'2021-05-01 19:10:30'),
			(37.0,1,'2021-05-01 19:10:35'),
			(31.0,1,'2021-05-01 19:10:40'),
			(33.0,1,'2021-05-02 19:10:45'),
      (30.0,1,'2021-05-03 19:10:45'),
      (31.0,1,'2021-05-04 8:10:45'),
      (30.0,1,'2021-05-04 19:10:45'),
      (31.0,1,'2021-05-05 19:10:45'),
      (34.0,1,'2021-05-06 19:10:45'),
			-- id 2
			(30.0,2,'2021-05-01 19:10:25'),
			(36.0,2,'2021-05-01 19:10:30'),
			(37.0,2,'2021-05-01 19:10:35'),
			(31.0,2,'2021-05-01 19:10:40'),
			(33.0,2,'2021-05-01 19:10:45'),
			--id 3
			(30.0,3,'2021-05-01 19:10:25'),
			(36.0,3,'2021-05-01 19:10:30'),
			(37.0,3,'2021-05-01 19:10:35'),
			(31.0,3,'2021-05-01 19:10:40'),
			(33.0,3,'2021-05-01 19:10:45'),
			--id 4
			(30.0,4,'2021-05-01 19:10:25'),
			(36.0,4,'2021-05-01 19:10:30'),
			(37.0,4,'2021-05-01 19:10:35'),
			(31.0,4,'2021-05-01 19:10:40'),
			(33.0,4,'2021-05-01 19:10:45');


-- humid------------------------------------------------------
-- record
CREATE TABLE humid_records(
	humidid SERIAL PRIMARY KEY,
	humidity FLOAT,
	roomid INT,
	datetime TIMESTAMP,
	CONSTRAINT fk_roomled
      FOREIGN KEY(roomid)
	  REFERENCES rooms(roomid)
	  ON DELETE SET NULL
);

INSERT INTO humid_records(humidity, roomid, datetime)
		VALUES(60.0,1,'2021-05-01 19:10:25'),
		(70.0,1,'2021-05-01 19:10:30'),
		(66.0,1,'2021-05-01 19:10:35'),
		(68.0,1,'2021-05-01 19:10:40'),
		(66.0,1,'2021-05-01 19:10:45'),
    (66.0,1,'2021-05-02 19:10:45'),
    (50.0,1,'2021-05-02 19:10:45'),
    (69.0,1,'2021-05-03 19:10:45'),
    (75.0,1,'2021-05-04 8:10:45'),
    (80.0,1,'2021-05-04 19:10:45'),
    (70.0,1,'2021-05-05 19:10:45'),
    (60.0,1,'2021-05-06 19:10:45'),


		--id 2
		(60.0,2,'2021-05-01 19:10:25'),
		(70.0,2,'2021-05-01 19:10:30'),
		(66.0,2,'2021-05-01 19:10:35'),
		(68.0,2,'2021-05-01 19:10:40'),
		(66.0,2,'2021-05-01 19:10:45'),

		--id 3
		(60.0,3,'2021-05-01 19:10:25'),
		(70.0,3,'2021-05-01 19:10:30'),
		(66.0,3,'2021-05-01 19:10:35'),
		(68.0,3,'2021-05-01 19:10:40'),
		(66.0,3,'2021-05-01 19:10:45'),
		--id4
		(60.0,4,'2021-05-01 19:10:25'),
		(70.0,4,'2021-05-01 19:10:30'),
		(66.0,4,'2021-05-01 19:10:35'),
		(68.0,4,'2021-05-01 19:10:40'),
		(66.0,4,'2021-05-01 19:10:45')
		;


--- Action ----------------------------------------------------------

CREATE TABLE action(
	aid SERIAL PRIMARY KEY,
	actionname VARCHAR(255) NOT NULL,
	roomid int,
	id int NOT NULL,
	datetime timestamp,
	CONSTRAINT fk_roomled
      FOREIGN KEY(roomid)
	  REFERENCES rooms(roomid)
	  ON DELETE SET NULL,
	CONSTRAINT fk_id
      FOREIGN KEY(id)
	  REFERENCES accounts(id)
	  ON DELETE SET NULL
)
;
INSERT INTO action(actionname, roomid, id, datetime)
	VALUES
	('Xac nhan canh bao khi gas',4,1,'2021-05-01 19:10:25'),
	('Xac nhan canh bao cua bi mo',1,1,'2021-05-01 19:10:25')
	,
	('Bat den led',1,1,'2021-05-01 19:10:25'),
	('Bat den led',2,2,'2021-05-01 19:15:25'),
	('Tat den led',2,2,'2021-05-01 20:15:25')
	;

-- Gas warning log

--delete from action
--where 1=1;

CREATE TABLE gaslog(
	gaslogid SERIAL PRIMARY KEY,
	datetime timestamp,
	aid int,
	CONSTRAINT fk_aid
      FOREIGN KEY(aid)
	  REFERENCES action(aid)
	  ON DELETE SET NULL
)
;
INSERT INTO gaslog(aid, datetime)
	VALUES(1,'2021-05-01 19:10:25')
	;

-- Door warning log

CREATE TABLE doorlog(
	doorlogid SERIAL PRIMARY KEY,
	datetime timestamp,
	aid int,
	CONSTRAINT fk_aid2
      FOREIGN KEY(aid)
	  REFERENCES action(aid)
	  ON DELETE SET NULL
)
;
INSERT INTO doorlog(aid,datetime)
	VALUES(2,'2021-05-01 19:10:25')
	;

-- control led

CREATE TABLE controlled(
	cledid SERIAL PRIMARY KEY,
	datetime timestamp,
	aid int,
	CONSTRAINT fk_aid
      FOREIGN KEY(aid)
	  REFERENCES action(aid)
	  ON DELETE SET NULL
)
;
INSERT INTO controlled(aid,datetime)
	VALUES(3,'2021-05-01 19:10:25'),
	(4,'2021-05-01 19:15:25'),
	(5,'2021-05-01 20:15:25')
	;


