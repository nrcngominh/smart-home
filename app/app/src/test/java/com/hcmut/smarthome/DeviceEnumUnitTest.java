package com.hcmut.smarthome;

import com.hcmut.smarthome.api.mqtt.GasWarningLevel;
import com.hcmut.smarthome.api.mqtt.LedMode;
import com.hcmut.smarthome.api.mqtt.MagneticMode;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeviceEnumUnitTest {
    @Test
    public void testLedMode_shouldReturnCorrectValue() {
        assertEquals(LedMode.OFF.getData(), "0");
        assertEquals(LedMode.RED.getData(), "1");
        assertEquals(LedMode.BLUE.getData(), "2");
    }

    @Test
    public void testGasWarningLevel_shouldReturnCorrectValue() {
        assertEquals(GasWarningLevel.SAFE.getData(), "0");
        assertEquals(GasWarningLevel.DANGER.getData(), "1");
    }


    @Test
    public void testMagneticMode_shouldReturnCorrectValue() {
        assertEquals(MagneticMode.OFF.getData(), "0");
        assertEquals(MagneticMode.ON.getData(), "1");
    }
}
