package com.hcmut.smarthome;

import com.hcmut.smarthome.api.mqtt.Dht11Data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Dht11DataUnitTest {
    @Test
    public void testParseDHT11Response_shouldParse() {
        Dht11Data data = Dht11Data.parse("31-60");
        assertEquals(data.getTemperature(), 31);
        assertEquals(data.getHumidity(), 60);
    }
}
