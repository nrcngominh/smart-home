package com.hcmut.smarthome.api.rest.fcm_registration;

public class RegistrationRequest {

    String token;

    public RegistrationRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
