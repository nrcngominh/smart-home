package com.hcmut.smarthome.features.main.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.messaging.FirebaseMessaging;
import com.hcmut.smarthome.R;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.MqttServiceImpl;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationAPI;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationRequest;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationResponse;
import com.hcmut.smarthome.api.rest.RetrofitClient;
import com.hcmut.smarthome.databinding.FragmentHomeBinding;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        MqttService mqttService = new MqttServiceImpl(getContext());
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.setupMqttService(mqttService);
        homeViewModel.getLastLightStatus();
        homeViewModel.getLastTempHumid();
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_home,
            container,
            false);
        binding.setViewModel(homeViewModel);
        binding.setLifecycleOwner(this);

        Thread mqttThread = new Thread(() -> {
            try {
                mqttService.connect(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        try {
                            mqttService.subscribe(Topics.getDht11Topic(), null);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.d("MQTT", "Connect failed");
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
        mqttThread.start();

        // Send registration token to server
        FirebaseMessaging.getInstance().getToken()
            .addOnCompleteListener(task -> {
                if (!task.isSuccessful()) {
                    Log.w("FCM", "Fetching FCM registration token failed", task.getException());
                    return;
                }

                String token = task.getResult();
                Log.w("FCM", token);

                // Send to server
                Retrofit retrofit = RetrofitClient.getInstance().getClient();
                RegistrationRequest req = new RegistrationRequest(token);

                Call<RegistrationResponse> call = retrofit.create(RegistrationAPI.class).sendToken(req);
                call.enqueue(new Callback<RegistrationResponse>() {
                    @Override
                    public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d("FCM", "Register successful");
                        } else {
                            Log.d("FCM", "Register failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                        Log.d("LOGIN", t.getMessage());
                    }
                });
            });

        return binding.getRoot();
    }
}
