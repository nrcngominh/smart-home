package com.hcmut.smarthome.api.mqtt;

public class FeedDataBuilder {
    FeedData feedData = new FeedData();

    public void reset() {
        feedData = new FeedData();
    }

    public void setLedData(LedMode ledMode) {
        feedData.setId("1");
        feedData.setName("LED");
        feedData.setData(ledMode.getData());
        feedData.setUnit("");
    }

    public void setBuzzerData(int volume) {
        feedData.setId("3");
        feedData.setName("SPEAKER");
        feedData.setData(Integer.toString(volume));
        feedData.setUnit("");
    }

    public void setLcdData(String message) {
        feedData.setId("5");
        feedData.setName("LCD");
        feedData.setData(message);
        feedData.setUnit("");
    }

    public FeedData build() {
        return feedData;
    }
}
