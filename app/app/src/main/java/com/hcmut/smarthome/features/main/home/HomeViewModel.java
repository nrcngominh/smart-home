package com.hcmut.smarthome.features.main.home;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hcmut.smarthome.Config;
import com.hcmut.smarthome.api.mqtt.Dht11Data;
import com.hcmut.smarthome.api.mqtt.FeedData;
import com.hcmut.smarthome.api.mqtt.FeedDataBuilder;
import com.hcmut.smarthome.api.mqtt.LedMode;
import com.hcmut.smarthome.api.mqtt.MqttHandler;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.api.rest.data_log.RawLogData;
import com.hcmut.smarthome.api.rest.led_state.LastLedState;
import com.hcmut.smarthome.data.TempAndHumid;
import com.hcmut.smarthome.features.draw_chart.DrawChartActivity;
import com.hcmut.smarthome.services.DataLogService;
import com.hcmut.smarthome.services.LedService;

import org.eclipse.paho.client.mqttv3.MqttException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {
    public static final String TAG = HomeViewModel.class.getSimpleName();
    private final Calendar cldr = Calendar.getInstance();
    private LedService ledService = new LedService();
    private DataLogService dataLogService = new DataLogService();
    private MutableLiveData<LedMode> lightStatus = new MutableLiveData<>(LedMode.OFF);
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);
    MutableLiveData<String> currentTemperature = new MutableLiveData<>("loading");
    MutableLiveData<String> currentHumid = new MutableLiveData<>("loading");

    MqttService mqttService;
    FeedDataBuilder builder = new FeedDataBuilder();
    Gson gson = new Gson();

    public void setupMqttService(MqttService service) {
        this.mqttService = service;
        mqttService.on(Topics.getDht11Topic(), new MqttHandler() {
            @Override
            public void handle(Object payload) {
                Gson gson = new Gson();
                FeedData data = gson.fromJson((String) payload, FeedData.class);
                Dht11Data dht11Data = Dht11Data.parse(data.getData());
                updateHumid(dht11Data.getHumidity());
                updateTemperature(dht11Data.getTemperature());
            }
        });
    }

    public void onTurnLedOffClicked() {
        Log.d(TAG, "Turn LED off");
        switchLed(LedMode.OFF);
    }

    public void onTurnLedRedClicked() {
        Log.d(TAG, "Turn LED red");
        switchLed(LedMode.RED);
    }

    public void onTurnLedBlueClicked() {
        Log.d(TAG, "Turn LED red");
        switchLed(LedMode.BLUE);
    }

    private void switchLed(LedMode ledMode) {
        try {
            builder.reset();
            builder.setLedData(ledMode);
            String msg = gson.toJson(builder.build());
            this.mqttService.publish(Topics.getLedTopic(), msg);
            Log.w("LED", msg);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void updateTemperature(double value) {
        Log.d(TAG, "New temperature value received");
        currentTemperature.postValue("" + value + "\u2103");
    }

    public void updateHumid(double value) {
        Log.d(TAG, "New humid value received");
        currentHumid.postValue("" + value + "%");
    }

    public MutableLiveData<String> getCurrentTemperature() {
        return currentTemperature;
    }

    public MutableLiveData<String> getCurrentHumid() {
        return currentHumid;
    }

    public MutableLiveData<LedMode> getLightStatus() {
        return lightStatus;
    }

    public void setLightStatus(MutableLiveData<LedMode> lightStatus) {
        this.lightStatus = lightStatus;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(MutableLiveData<Boolean> isLoading) {
        this.isLoading = isLoading;
    }

    public void getLastLightStatus(){
        ledService.getLastLedState(Config.USERNAME1, "bk-iot-led", Config.PASSWORD1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<LastLedState>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    isLoading.postValue(true);
                }

                @Override
                public void onNext(@NonNull LastLedState lastLedState) {
                    isLoading.postValue(false);
                    switch (lastLedState.getLedState().getData()){
                        case "0":
                            lightStatus.postValue(LedMode.OFF);
                            break;
                        case "1":
                            lightStatus.postValue(LedMode.RED);
                            break;
                        case "2":
                            lightStatus.postValue(LedMode.BLUE);
                            break;
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    isLoading.postValue(false);
                }

                @Override
                public void onComplete() {
                    isLoading.postValue(false);
                }
            });
    }

    public void getLastTempHumid() {
        long from = getLastNDaysTime(0);
        long to = System.currentTimeMillis() / 1000;
        Log.d("Time", "From: " + from + " To: " + to);
        // Call service here
        callApi(from, to, "Today temperature log", DrawChartActivity.TEMPERATURE, "HH:mm");

    }

    private long getLastNDaysTime(int days) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy", Locale.US);
            int cDay = cldr.get(Calendar.DAY_OF_MONTH);
            int cMonth = cldr.get(Calendar.MONTH);
            int cYear = cldr.get(Calendar.YEAR);
            Date currentDate = simpleDateFormat.parse("" + cDay + "/" + (cMonth + 1) + "/" + cYear);
            return (currentDate.getTime() - days * 24 * 60 * 60 * 1000) / 1000 + 60;
        } catch (ParseException e) {
            Log.d("GetDateTime", "Fail to calculate date time");
            return System.currentTimeMillis() / 1000;
        }
    }

    private void callApi(long from, long to, String title, String type, String timeFormat) {
        dataLogService.getDataLog(from, to)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<RawLogData>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull List<RawLogData> rawLogData) {
                    Log.d("DrawChart", "OnNext");
                    if (rawLogData.isEmpty()){
                        Log.d("DrawChart", "No data");
                    }
                    else {
                        Log.d("DrawChart", "There's " + rawLogData.size());
                        ArrayList<TempAndHumid> parsedData = new ArrayList<>();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        if (rawLogData.size() > 0) {
                            RawLogData latestData = rawLogData.get(rawLogData.size() - 1);
                            updateTemperature(latestData.getTemperature());
                            updateHumid(latestData.getHumidity());
                        }
                    }

                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Log.d("DrawChart", "onError");
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
    }

}
