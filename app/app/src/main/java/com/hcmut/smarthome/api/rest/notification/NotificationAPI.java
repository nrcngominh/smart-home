package com.hcmut.smarthome.api.rest.notification;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface NotificationAPI {
    @GET("api/gas-notification")
    Observable<GasNotification> getGasNotificationStatus();

    @POST("api/gas-notification")
    Observable<Void> setGasNotificationStatus(@Body GasNotification status);

    @GET("api/door-notification")
    Observable<DoorNotification> getDoorNotificationStatus();

    @POST("api/door-notification")
    Observable<Void> setDoorNotificationStatus(@Body DoorNotification status);
}
