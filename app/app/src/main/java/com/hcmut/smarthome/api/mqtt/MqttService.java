package com.hcmut.smarthome.api.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.MqttException;

public interface MqttService {
    void on(String topic, MqttHandler handler, IMqttActionListener actionListener);

    void on(String topic, MqttHandler handler);

    void off(String topic);

    void publish(String topic, Object data) throws MqttException;

    void subscribe(String topic, IMqttActionListener listener) throws MqttException;

    void connect(IMqttActionListener listener) throws MqttException;
}
