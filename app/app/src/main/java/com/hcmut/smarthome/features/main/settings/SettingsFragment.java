package com.hcmut.smarthome.features.main.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.databinding.FragmentSettingsBinding;
import com.hcmut.smarthome.features.door.DoorViewModel;


public class SettingsFragment extends Fragment {

    private SettingsViewModel viewModel;
    private FragmentSettingsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_settings,
            container,
            false
        );
        viewModel = new SettingsViewModel();
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);


        return binding.getRoot();
    }
}
