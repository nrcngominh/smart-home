package com.hcmut.smarthome.api.rest.led_state;


import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Path;


public interface LedStateAPI {
    @GET("/api/v2/{username}/feeds/{feed_key}/data/last?include=value")
    Observable<LastLedState> getLastLedState(@Path("username") String username,
                                             @Path("feed_key") String feed_key,
                                             @Header("X-AIO-Key") String io_key);
}
