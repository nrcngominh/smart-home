package com.hcmut.smarthome;

public class Config {
    public static String SERVER_HOST = BuildConfig.SERVER_HOST;
    public static String BROKER_HOST = BuildConfig.BROKER_HOST;
    public static String USERNAME1 = BuildConfig.USERNAME1;
    public static String PASSWORD1 = BuildConfig.PASSWORD1;
    public static String USERNAME2 = BuildConfig.USERNAME2;
    public static String PASSWORD2 = BuildConfig.PASSWORD2;
    public static String PREFIX1 = BuildConfig.PREFIX1;
    public static String PREFIX2 = BuildConfig.PREFIX2;
}
