package com.hcmut.smarthome.features.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.databinding.ActivitySignInBinding;
import com.hcmut.smarthome.features.authentication.events.SignInRequestEvent;
import com.hcmut.smarthome.features.authentication.events.SignInResponseEvent;
import com.hcmut.smarthome.features.door.DoorActivity;
import com.hcmut.smarthome.features.gas.GasActivity;
import com.hcmut.smarthome.features.main.MainActivity;
import com.hcmut.smarthome.services.AccountService;
import com.hcmut.smarthome.services.AccountServiceFake;
import com.hcmut.smarthome.services.AccountServiceImpl;
import com.hcmut.smarthome.utils.SharedPreferencesHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SignInActivity extends AppCompatActivity {
    public static final String SHOULD_FINISH = "should_finish";
    private static final String TAG = "SignInActivity";
    private ActivitySignInBinding binding;
    private SignInViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);

        // Dependencies
        //AccountService accountService = new AccountServiceFake();
        AccountService accountService = new AccountServiceImpl();
        viewModel = new SignInViewModel(accountService);

        // Binding
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        // Notification
        if (getIntent().getExtras() != null && getIntent().getBooleanExtra(SHOULD_FINISH, false)) {
            // User exit app
            finish();
        }
        else
            checkAlreadySignIn();
    }

    private void checkAlreadySignIn() {
        if (SharedPreferencesHelper.getSignInStatus(this)){
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtras(getIntent());
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onSignInRequestEvent(SignInRequestEvent event) {
        String username = binding.edtUsername.getText().toString();
        String password = binding.edtPassword.getText().toString();
        viewModel.validateUser(username, password);
    }

    @Subscribe
    public void onSignInResponseEvent(SignInResponseEvent event) {
        if (event.isSuccessful()) {
            SharedPreferencesHelper.setSignInStatus(this, true);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

}
