package com.hcmut.smarthome.api.mqtt;

import android.content.Context;
import android.util.Log;

import com.hcmut.smarthome.Config;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

public class MqttServiceImpl implements MqttService {
    final String serverUri = "tcp://" + Config.BROKER_HOST + ":1883";

    final String clientId1 = String.valueOf(UUID.randomUUID());
    final String username1 = Config.USERNAME1;
    final String password1 = Config.PASSWORD1;

    final String clientId2 = String.valueOf(UUID.randomUUID());
    final String username2 = Config.USERNAME2;
    final String password2 = Config.PASSWORD2;

    public MqttAndroidClient mqttAndroidClient1;
    public MqttAndroidClient mqttAndroidClient2;

    public HashMap<String, LinkedList<MqttHandler>> topicHandlers = new HashMap<>();

    public MqttServiceImpl(Context context) {
        Log.w("MQTT", "Connect to " + serverUri);

        mqttAndroidClient1 = new MqttAndroidClient(context, serverUri, clientId1);
        mqttAndroidClient2 = new MqttAndroidClient(context, serverUri, clientId2);

        mqttAndroidClient1.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.w("MQTT", "Connect complete");
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.w("MQTT", "Connection lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d("MQTT", "Recv: " + message.toString());
                LinkedList<MqttHandler> handlers = topicHandlers.get(topic);

                if (handlers != null) {
                    for (MqttHandler handler : handlers) {
                        handler.handle(message.toString());
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                Log.d("MQTT", "Send: " + token.toString());
            }
        });
        mqttAndroidClient2.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.w("MQTT", "Connect complete");
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.w("MQTT", "Connection lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d("MQTT", "Recv: " + message.toString());
                LinkedList<MqttHandler> handlers = topicHandlers.get(topic);

                if (handlers != null) {
                    for (MqttHandler handler : handlers) {
                        handler.handle(message.toString());
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                Log.d("MQTT", "Send: " + token.toString());
            }
        });
    }

    @Override
    public void on(String topic, MqttHandler handler, IMqttActionListener actionListener) {
        // Add handler
        if (topicHandlers.get(topic) == null) {
            topicHandlers.put(topic, new LinkedList<MqttHandler>());
        }
        topicHandlers.get(topic).add(handler);
    }

    @Override
    public void on(String topic, MqttHandler handler) {
        on(topic, handler, null);
    }

    @Override
    public void off(String topic) {
        topicHandlers.remove(topic);
    }

    @Override
    public void subscribe(String topic, IMqttActionListener listener) throws MqttException {
        if (topic.equals(Topics.getGasTopic())) {
            mqttAndroidClient2.subscribe(topic, 0, null, listener);
        } else {
            mqttAndroidClient1.subscribe(topic, 0, null, listener);
        }
    }

    @Override
    public void publish(String topic, Object data) throws MqttException {
        byte[] payload = data.toString().getBytes();
        MqttMessage message = new MqttMessage(payload);

        if (topic.equals(Topics.getGasTopic())) {
            mqttAndroidClient2.publish(topic, message);
        } else {
            mqttAndroidClient1.publish(topic, message);
        }
    }

    @Override
    public void connect(IMqttActionListener listener) throws MqttException {
        MqttConnectOptions mqttConnectOptions1 = new MqttConnectOptions();
        mqttConnectOptions1.setAutomaticReconnect(true);
        mqttConnectOptions1.setCleanSession(false);

        MqttConnectOptions mqttConnectOptions2 = new MqttConnectOptions();
        mqttConnectOptions2.setAutomaticReconnect(true);
        mqttConnectOptions2.setCleanSession(false);

        if(!this.username1.isEmpty()){
            mqttConnectOptions1.setUserName(this.username1);
            mqttConnectOptions1.setPassword(this.password1.toCharArray());
        }
        if(!this.username2.isEmpty()){
            mqttConnectOptions2.setUserName(this.username2);
            mqttConnectOptions2.setPassword(this.password2.toCharArray());
        }

        mqttAndroidClient1.connect(mqttConnectOptions1, null, listener);
        mqttAndroidClient2.connect(mqttConnectOptions2, null, listener);
    }
}
