package com.hcmut.smarthome.api.rest.data_log;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DataLogAPI {
    @GET("api/temp-humid")
    Observable<List<RawLogData>> getDataLogs(@Query("from") long from, @Query("to") long to);
}
