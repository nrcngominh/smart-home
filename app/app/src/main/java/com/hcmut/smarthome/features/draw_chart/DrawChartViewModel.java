package com.hcmut.smarthome.features.draw_chart;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.hcmut.smarthome.api.rest.data_log.RawLogData;
import com.hcmut.smarthome.data.TempAndHumid;
import com.hcmut.smarthome.services.DataLogService;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class DrawChartViewModel extends ViewModel {
    public static final int MIN_TIME_DIFF = 300;
    private DataLogService dataLogService = new DataLogService();
    private final Calendar cldr = Calendar.getInstance();
    WeakReference<DrawChartActivity> drawChartActivity;
    public void fetchTodayTemperature(){
        long from = getLastNDaysTime(0);
        long to = System.currentTimeMillis() / 1000;
        Log.d("Time", "From: " + from + " To: " + to);
        // Call service here
        callApi(from, to, "Today temperature log", DrawChartActivity.TEMPERATURE, "HH:mm");

        // Fake data
        // ArrayList<TempAndHumid> data = new ArrayList<>();
        // for (long i = from; i <= to; i += 300){
        //     data.add(new TempAndHumid(
        //         20 + Math.random() * 15,
        //         30 + Math.random() * 65,
        //         i
        //     ));
        // }

        // drawChartActivity.get().drawChart(
        //     "Today temperature log",
        //     DrawChartActivity.TEMPERATURE,
        //     "HH:mm",
        //     data);
    }

    public void fetchTodayHumidity(){
        long from = getLastNDaysTime(0);
        long to = System.currentTimeMillis() / 1000;
        Log.d("Time", "From: " + from + " To: " + to);
        // Call service here
        callApi(from, to, "Today humidity log", DrawChartActivity.HUMIDITY, "HH:mm");
        // Fake data
        // ArrayList<TempAndHumid> data = new ArrayList<>();
        // for (long i = from; i <= to; i += 300){
        //     data.add(new TempAndHumid(
        //         25 + Math.random() * 8,
        //         60 + Math.random() * 25,
        //         i
        //     ));
        // }
        // drawChartActivity.get().drawChart(
        //     "Today humidity log",
        //     DrawChartActivity.HUMIDITY,
        //     "HH:mm",
        //     data);
    }

    public void fetchPastTemperature(){
        // Call service here
        //long from = 1619936079;
        //long to = 1620368079;
        long from = getLastNDaysTime(7);
        long to = System.currentTimeMillis() / 1000;
        callApi(from, to, "7 days temperature log", DrawChartActivity.TEMPERATURE, "HH:mm dd/M");
        //region Fake data
//        long from = getLastNDaysTime(7);
//        long to = System.currentTimeMillis() / 1000;
//        ArrayList<TempAndHumid> data = new ArrayList<>();
//        for (long i = from; i <= to; i += 1800){
//            data.add(new TempAndHumid(
//                25 + Math.random() * 8,
//                60 + Math.random() * 25,
//                i
//            ));
//        }

//        drawChartActivity.get().drawChart(
//            "7 days temperature log",
//            DrawChartActivity.TEMPERATURE,
//            "HH:mm dd/M",
//            data);
        //endregion
    }

    public void fetchPastHumidity(){
        long from = getLastNDaysTime(7);
        long to = System.currentTimeMillis() / 1000;
        // Call service here
         callApi(from, to, "7 days humidity log", DrawChartActivity.HUMIDITY, "HH:mm dd/M");
        // Fake data
        // ArrayList<TempAndHumid> data = new ArrayList<>();
        // for (long i = from; i <= to; i += 1800){
        //     data.add(new TempAndHumid(
        //         25 + Math.random() * 8,
        //         60 + Math.random() * 25,
        //         i
        //     ));
        // }

        // drawChartActivity.get().drawChart(
        //     "7 days humidity log",
        //     DrawChartActivity.HUMIDITY,
        //     "HH:mm dd/M",
        //     data);
    }

    private long getLastNDaysTime(int days) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy", Locale.US);
            int cDay = cldr.get(Calendar.DAY_OF_MONTH);
            int cMonth = cldr.get(Calendar.MONTH);
            int cYear = cldr.get(Calendar.YEAR);
            Date currentDate = simpleDateFormat.parse("" + cDay + "/" + (cMonth + 1) + "/" + cYear);
            return (currentDate.getTime() - days * 24 * 60 * 60 * 1000) / 1000 + 60;
        } catch (ParseException e) {
            Log.d("GetDateTime", "Fail to calculate date time");
            return System.currentTimeMillis() / 1000;
        }
    }

    private void callApi(long from, long to, String title, String type, String timeFormat) {
        dataLogService.getDataLog(from, to)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<RawLogData>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull List<RawLogData> rawLogData) {
                    Log.d("DrawChart", "OnNext");
                    if (rawLogData.isEmpty()){
                        Log.d("DrawChart", "No data");

                    }
                    else {
                        Log.d("DrawChart", "There's " + rawLogData.size());
                        ArrayList<TempAndHumid> parsedData = new ArrayList<>();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        for (RawLogData rawData : rawLogData){
                            try {
                                Date parsedDate = dateFormat.parse(rawData.getDatetime());
                                Log.d("DrawChart", "" + parsedDate.getTime());
                                // Get data once every 5 minutes
                                if (parsedData.isEmpty() || (parsedDate.getTime()/1000 - parsedData.get(parsedData.size() - 1).getDatetime() > MIN_TIME_DIFF)) {
                                    parsedData.add(new TempAndHumid(
                                        rawData.getTemperature(),
                                        rawData.getHumidity(),
                                        parsedDate.getTime() / 1000
                                    ));
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("DrawChart", "" + parsedData.size());
                        drawChartActivity.get().drawChart(title, type, timeFormat, parsedData);
                    }

                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Log.d("DrawChart", "onError");
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
    }

    public void setDrawChartActivity(WeakReference<DrawChartActivity> drawChartActivity) {
        this.drawChartActivity = drawChartActivity;
    }

    public void onBackButtonClicked(){
        drawChartActivity.get().goBack();
    }
}
