package com.hcmut.smarthome.services;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hcmut.smarthome.R;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationAPI;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationRequest;
import com.hcmut.smarthome.api.rest.fcm_registration.RegistrationResponse;
import com.hcmut.smarthome.api.rest.RetrofitClient;
import com.hcmut.smarthome.features.authentication.SignInActivity;
import com.hcmut.smarthome.features.door.DoorActivity;
import com.hcmut.smarthome.features.gas.GasActivity;
import com.hcmut.smarthome.utils.NotificationUtil;

import org.jetbrains.annotations.NotNull;

import java.io.Console;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NonNull String token) {
        Log.d("FCM", token);

        // Send registration key to server
        sendRegistrationToServer(token);
    }

    @Override
    public void onMessageReceived(@NonNull @NotNull RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            //Log.d("FCM",remoteMessage.getNotification().get);

            String title = remoteMessage.getNotification().getTitle();
            String description = remoteMessage.getNotification().getBody();
            int notificationId = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
            // Check if it's door or gas warning
            String type = "";
            try {
                type = description.split(" ")[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent main = new Intent(this, SignInActivity.class);
            if (type.equals("Gas")) {
                main = new Intent(this, GasActivity.class);
                main.putExtra(GasActivity.GAS_WARNING, true);
            }
            else if (type.equals("Door")) {
                main = new Intent(this, DoorActivity.class);
                main.putExtra(DoorActivity.DOOR_WARNING, true);
            }
            main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, main, 0);
            NotificationUtil.createNotification(this,
                notificationId,
                R.drawable.app_logo,
                title,
                description,
                NotificationCompat.PRIORITY_DEFAULT,
                true,
                pendingIntent
            );
        }
    }
    public void sendRegistrationToServer(String token) {
        Retrofit retrofit = RetrofitClient.getInstance().getClient();
        RegistrationRequest req = new RegistrationRequest(token);

        Call<RegistrationResponse> call = retrofit.create(RegistrationAPI.class).sendToken(req);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("FCM", "Register successful");
                } else {
                    Log.d("FCM", "Register failed");
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                Log.d("LOGIN", t.getMessage());
            }
        });
    }
}
