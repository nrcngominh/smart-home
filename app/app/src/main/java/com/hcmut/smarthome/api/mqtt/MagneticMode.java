package com.hcmut.smarthome.api.mqtt;

public enum MagneticMode {
    OFF("0"),
    ON("1");

    final String data;

    public String getData() {
        return data;
    }

    MagneticMode(String data) {
        this.data = data;
    }
}
