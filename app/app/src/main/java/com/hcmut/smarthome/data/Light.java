package com.hcmut.smarthome.data;

public class Light {
    public static final String LIVING_ROOM_LIGHT = "1";
    public static final String BEDROOM_LIGHT = "2";
    public static final String KITCHEN_LIGHT = "3";
    public static final String TOILET_LIGHT = "4";
}
