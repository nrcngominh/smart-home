package com.hcmut.smarthome.api.rest.fcm_registration;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RegistrationAPI {
    @POST("api/registration-token/")
    Call<RegistrationResponse> sendToken(@Body RegistrationRequest req);
}
