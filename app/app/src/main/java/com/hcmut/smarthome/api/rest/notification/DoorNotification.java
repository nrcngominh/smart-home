package com.hcmut.smarthome.api.rest.notification;

public class DoorNotification {
    private String door;

    public DoorNotification(String door) {
        this.door = door;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }
}
