package com.hcmut.smarthome.api.mqtt;

public enum GasWarningLevel {
    SAFE("0"),
    DANGER("1");

    final String data;

    public String getData() {
        return data;
    }

    GasWarningLevel(String data) {
        this.data = data;
    }
}
