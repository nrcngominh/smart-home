package com.hcmut.smarthome.features.draw_chart;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.hcmut.smarthome.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CustomMarker extends MarkerView {

    private TextView tvContent;
    private String unit;
    private String timeFormat;
    private TextView tvDetail;

    public void setTvDetail(TextView tvDetail) {
        this.tvDetail = tvDetail;
    }

    public CustomMarker(Context context, int layoutResource, String unit, String timeFormat) {
        super(context, layoutResource);
        this.unit = unit;
        this.timeFormat = timeFormat;
        // find your layout components
        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
// content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, Locale.US);
        String dateTime = sdf.format(new Date((long) (e.getX()*1000)));
        tvDetail.setText(String.format(Locale.US,"%.2f%s at %s", e.getY(), unit, dateTime));
        //tvContent.setText("" + roundedValue + unit + " at " + dateTime);

        // this will perform necessary layouting
        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;

    @Override
    public MPPointF getOffset() {

        if(mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mOffset;
    }}
