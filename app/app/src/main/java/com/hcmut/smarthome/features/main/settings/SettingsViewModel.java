package com.hcmut.smarthome.features.main.settings;

import androidx.lifecycle.ViewModel;

import org.greenrobot.eventbus.EventBus;

public class SettingsViewModel extends ViewModel {

    public void onSignOutClicked(){
        EventBus.getDefault().post(new SignOutEvent());
    }

}
