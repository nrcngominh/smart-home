package com.hcmut.smarthome.features.draw_chart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.hcmut.smarthome.R;
import com.hcmut.smarthome.data.TempAndHumid;
import com.hcmut.smarthome.databinding.ActivityDrawChartBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DrawChartActivity extends AppCompatActivity {
    public static final String DATA_TYPE = "data_type";
    public static final String TEMPERATURE = "temperature";
    public static final String HUMIDITY = "humidity";
    public static final String TIME_TYPE = "time_type";
    public static final String TODAY = "today";
    public static final String PAST = "past";

    private ActivityDrawChartBinding binding;
    private DrawChartViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_draw_chart);
        viewModel = new ViewModelProvider(this).get(DrawChartViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        viewModel.setDrawChartActivity(new WeakReference<>(this));
        String dataType = getIntent().getStringExtra(DATA_TYPE);
        String timeType = getIntent().getStringExtra(TIME_TYPE);

        chooseChartType(dataType, timeType);

        //drawChart("temperature", dataModels);
    }

    private void chooseChartType(String dataType, String timeType) {
        if (dataType.equals(TEMPERATURE) && timeType.equals(TODAY)){
            viewModel.fetchTodayTemperature();
        }
        else if (dataType.equals(TEMPERATURE) && timeType.equals(PAST)){
            viewModel.fetchPastTemperature();
        }
        else if (dataType.equals(HUMIDITY) && timeType.equals(TODAY)){
            viewModel.fetchTodayHumidity();
        }
        else if (dataType.equals(HUMIDITY) && timeType.equals(PAST)){
            viewModel.fetchPastHumidity();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onDrawChartEvent(DrawChartEvent event){
        drawChart(
            event.getTitle(),
            event.getType(),
            event.getTimeFormat(),
            event.getDataList()
        );
    }

    public void drawChart(String title, String type, String timeFormat, List<TempAndHumid> dataModels) {
        if (dataModels.isEmpty())
            return;
        binding.txtChartTitle.setText(title);
        LineChart lineChart = binding.lineChart;

        CustomMarker customMarker = new CustomMarker(this,
            R.layout.custom_marker,
            type.equals(TEMPERATURE) ? "°C" : "%",
            timeFormat);
        customMarker.setTvDetail(binding.tvDetail);

        lineChart.setMarker(customMarker);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.setNoDataText("No chart data available. Please tap to refresh");
        lineChart.invalidate();
        // set legend
        XAxis xAxis = lineChart.getXAxis();
        //xAxis.setAxisMinimum(dataModels.get(0).getTimestamp());
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                SimpleDateFormat sdf = new SimpleDateFormat(timeFormat, Locale.US);
                return sdf.format(new Date((long) (value*1000)));
            }
        });

        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                if (type.equals(TEMPERATURE))
                    return value + "°C";
                else
                    return value + "%";
            }
        });

        ArrayList<Entry> entries = new ArrayList<>();
        for (TempAndHumid data : dataModels)
            if (type.equals(TEMPERATURE))
                entries.add(new Entry(data.getDatetime(), (float) data.getTemperature()));
            else
                entries.add(new Entry(data.getDatetime(), (float) data.getHumidity()));

        LineDataSet dataSet = new LineDataSet(entries, title);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setDrawFilled(true);
        dataSet.setDrawValues(true);

        dataSet.setFillAlpha(110);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        dataSets.add(dataSet);

        LineData data = new LineData(dataSets);

        lineChart.setData(data);
    }

    public void goBack(){
        onBackPressed();
    }
}
