package com.hcmut.smarthome.data;

public class TempAndHumid {
    private double temperature;
    private double humidity;
    private long datetime;

    public TempAndHumid(double temperature, double humidity, long datetime) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.datetime = datetime;
    }

    public double getTemperature() {
        return temperature;
    }
    public double getHumidity() {
        return humidity;
    }
    public long getDatetime() {
        return datetime;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
}
