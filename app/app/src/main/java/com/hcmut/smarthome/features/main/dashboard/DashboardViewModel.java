package com.hcmut.smarthome.features.main.dashboard;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hcmut.smarthome.Config;
import com.hcmut.smarthome.api.mqtt.FeedDataBuilder;
import com.hcmut.smarthome.api.mqtt.LedMode;
import com.hcmut.smarthome.api.mqtt.MqttHandler;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.api.rest.led_state.LastLedState;
import com.hcmut.smarthome.features.door.DoorActivity;
import com.hcmut.smarthome.features.gas.GasActivity;
import com.hcmut.smarthome.features.main.dashboard.events.OpenLightDialogEvent;
import com.hcmut.smarthome.features.temperature.TempActivity;
import com.hcmut.smarthome.services.LedService;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class DashboardViewModel extends ViewModel {
    public static final String TAG = DashboardViewModel.class.getSimpleName();
    private LedService ledService = new LedService();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);
    private MutableLiveData<LedMode> lightStatus = new MutableLiveData<>(LedMode.OFF);
    MqttService mqttService;
    FeedDataBuilder builder = new FeedDataBuilder();
    Gson gson = new Gson();

    public void setupMqttService(MqttService service) {
        this.mqttService = service;
        mqttService.on(Topics.getDht11Topic(), new MqttHandler() {
            @Override
            public void handle(Object payload) {

            }
        });
    }

    public void switchLed(LedMode ledMode) {
        try {
            builder.reset();
            builder.setLedData(ledMode);
            String msg = gson.toJson(builder.build());
            this.mqttService.publish(Topics.getLedTopic(), msg);
            Log.w("LED", msg);
            lightStatus.postValue(ledMode);
        } catch (MqttException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void onLightClicked(String status){
        EventBus.getDefault().post(new OpenLightDialogEvent(status));
    }

    public void onTempHumidityClicked(){
        EventBus.getDefault().post(new LoadActivityEvent(TempActivity.class));
    }

    public void onGasClicked(){
        EventBus.getDefault().post(new LoadActivityEvent(GasActivity.class));
    }

    public void onDoorClicked(){
        EventBus.getDefault().post(new LoadActivityEvent(DoorActivity.class));
    }

    public MutableLiveData<LedMode> getLightStatus() {
        return lightStatus;
    }

    public void setLightStatus(MutableLiveData<LedMode> lightStatus) {
        this.lightStatus = lightStatus;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(MutableLiveData<Boolean> isLoading) {
        this.isLoading = isLoading;
    }

    public void getLastLightStatus(){
        ledService.getLastLedState(Config.USERNAME1, "bk-iot-led", Config.PASSWORD1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<LastLedState>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    isLoading.postValue(true);
                }

                @Override
                public void onNext(@NonNull LastLedState lastLedState) {
                    isLoading.postValue(false);
                    switch (lastLedState.getLedState().getData()){
                        case "0":
                            lightStatus.postValue(LedMode.OFF);
                            break;
                        case "1":
                            lightStatus.postValue(LedMode.RED);
                            break;
                        case "2":
                            lightStatus.postValue(LedMode.BLUE);
                            break;
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    isLoading.postValue(false);
                }

                @Override
                public void onComplete() {
                    isLoading.postValue(false);
                }
            });
    }
}
