package com.hcmut.smarthome.services;

import android.util.Log;

import com.hcmut.smarthome.api.rest.account.AccountAPI;
import com.hcmut.smarthome.api.rest.account.LoginRequest;
import com.hcmut.smarthome.api.rest.account.LoginResponse;
import com.hcmut.smarthome.api.rest.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AccountServiceImpl implements AccountService {

    @Override
    public void login(String username, String password, AccountServiceObserver observer) {
        Retrofit retrofit = RetrofitClient.getInstance().getClient();
        LoginRequest req = new LoginRequest(username, password);

        Call<LoginResponse> call = retrofit.create(AccountAPI.class).login(req);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    observer.raise(true);
                    Log.d("LOGIN", "Login successful");
                } else {
                    observer.raise(false);
                    Log.d("LOGIN", "Login failed");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("LOGIN", t.getMessage());
            }
        });
    }
}
