package com.hcmut.smarthome.api.rest.account;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AccountAPI {
    @POST("api/login/")
    Call<LoginResponse> login(@Body LoginRequest req);
}
