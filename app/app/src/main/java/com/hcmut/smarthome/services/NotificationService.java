package com.hcmut.smarthome.services;

import com.hcmut.smarthome.api.rest.RetrofitClient;
import com.hcmut.smarthome.api.rest.notification.DoorNotification;
import com.hcmut.smarthome.api.rest.notification.GasNotification;
import com.hcmut.smarthome.api.rest.notification.NotificationAPI;

import io.reactivex.rxjava3.core.Observable;

public class NotificationService {
    public Observable<GasNotification> getGasNotificationStatus(){
        return RetrofitClient.getInstance()
            .getClient()
            .create(NotificationAPI.class)
            .getGasNotificationStatus();
    }
    public Observable<Void> setGasNotificationStatus(GasNotification status){
        return RetrofitClient.getInstance()
            .getClient()
            .create(NotificationAPI.class)
            .setGasNotificationStatus(status);
    }
    public Observable<DoorNotification> getDoorNotificationStatus(){
        return RetrofitClient.getInstance()
            .getClient()
            .create(NotificationAPI.class)
            .getDoorNotificationStatus();
    }
    public Observable<Void> setDoorNotificationStatus(DoorNotification status){
        return RetrofitClient.getInstance()
            .getClient()
            .create(NotificationAPI.class)
            .setDoorNotificationStatus(status);
    }
}
