package com.hcmut.smarthome.api.rest;

import com.hcmut.smarthome.Config;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static class SingletonHolder {
        public static RetrofitClient instance = new RetrofitClient();
    }

    private final Retrofit retrofit;

    private RetrofitClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(logging)
            .build();
        retrofit = new Retrofit.Builder()
            .baseUrl("http://" + Config.SERVER_HOST)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build();
    }

    public static RetrofitClient getInstance() {
        return SingletonHolder.instance;
    }

    public Retrofit getClient() {
        return retrofit;
    }

}
