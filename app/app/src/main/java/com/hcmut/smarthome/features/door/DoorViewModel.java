package com.hcmut.smarthome.features.door;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hcmut.smarthome.api.rest.notification.DoorNotification;
import com.hcmut.smarthome.api.rest.notification.GasNotification;
import com.hcmut.smarthome.features.gas.GasActivity;
import com.hcmut.smarthome.services.NotificationService;

import java.lang.ref.WeakReference;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class DoorViewModel extends ViewModel {
    public static final String TAG = DoorViewModel.class.getSimpleName();
    private WeakReference<DoorActivity> doorActivity;
    private MutableLiveData<String> doorLockStatus = new MutableLiveData<>("");
    private MutableLiveData<Boolean> doorLockTracking = new MutableLiveData<>(true);
    private NotificationService notificationService = new NotificationService();

    public void initialize(WeakReference<DoorActivity> doorActivity){
        doorLockStatus.postValue("Safe"); // fake
        this.doorActivity = doorActivity;
        getNotificationStatus();
    }

    private void getNotificationStatus() {
        notificationService.getDoorNotificationStatus()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<DoorNotification>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    doorActivity.get().showLoadingDialog();
                }

                @Override
                public void onNext(@NonNull DoorNotification doorNotification) {
                    doorActivity.get().hideLoadingDialog();
                    doorActivity.get().showNotificationStatus(
                        doorNotification.getDoor().equals("on")
                    );
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    doorActivity.get().hideLoadingDialog();
                    e.printStackTrace();
                    doorActivity.get().showToast("Can't get data from server");
                }

                @Override
                public void onComplete() {

                }
            });
    }

    public void turnDoorTrackingOn(){
        Log.d(TAG, "User turn door lock tracking on");
        setNotificationStatusApi("on");
    }

    public void turnDoorTrackingOff(){
        Log.d(TAG, "User turn door lock tracking off");
        setNotificationStatusApi("off");
    }

    private void setNotificationStatusApi(String status){
        notificationService.setDoorNotificationStatus(new DoorNotification(status))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Void>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    doorActivity.get().showLoadingDialog();
                }

                @Override
                public void onNext(@NonNull Void unused) {
                    doorActivity.get().hideLoadingDialog();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    doorActivity.get().hideLoadingDialog();
                    e.printStackTrace();
                    if (e.getClass() != NullPointerException.class)
                        doorActivity.get().showToast("Fail to update notification settings");
                }

                @Override
                public void onComplete() {

                }
            });
    }

    public MutableLiveData<String> getDoorLockStatus() {
        return doorLockStatus;
    }

    public void setDoorLockStatus(MutableLiveData<String> doorLockStatus) {
        this.doorLockStatus = doorLockStatus;
    }

    public MutableLiveData<Boolean> getDoorLockTracking() {
        return doorLockTracking;
    }

    public void setDoorLockTracking(MutableLiveData<Boolean> doorLockTracking) {
        this.doorLockTracking = doorLockTracking;
    }

    public void onBackButtonClicked(){
        doorActivity.get().goBack();
    }

}
