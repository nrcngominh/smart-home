package com.hcmut.smarthome.services;

import com.hcmut.smarthome.Config;
import com.hcmut.smarthome.api.rest.RetrofitClient;
import com.hcmut.smarthome.api.rest.led_state.LastLedState;
import com.hcmut.smarthome.api.rest.led_state.LedStateAPI;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class LedService {
    public Observable<LastLedState> getLastLedState(String username, String feed_key, String io_key){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(logging)
            .build();
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://io.adafruit.com")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build();
        return retrofit.create(LedStateAPI.class)
            .getLastLedState(username, feed_key, io_key);
    }
}
