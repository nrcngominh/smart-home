package com.hcmut.smarthome.features.gas;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hcmut.smarthome.api.rest.notification.GasNotification;
import com.hcmut.smarthome.services.NotificationService;

import java.lang.ref.WeakReference;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class GasViewModel extends ViewModel {
    public static final String TAG = GasViewModel.class.getSimpleName();
    private WeakReference<GasActivity> gasActivity;
    private MutableLiveData<String> gasStatus = new MutableLiveData<>("");
    private MutableLiveData<Boolean> gasSensorOn = new MutableLiveData<>(true);
    private NotificationService notificationService = new NotificationService();

    public void initialize(WeakReference<GasActivity> gasActivity){
        gasStatus.postValue("Safe"); // fake
        this.gasActivity = gasActivity;
        getNotificationStatus();
    }

    private void getNotificationStatus() {
        notificationService.getGasNotificationStatus()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<GasNotification>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    gasActivity.get().showLoadingDialog();
                }

                @Override
                public void onNext(@NonNull GasNotification gasNotification) {
                    gasActivity.get().hideLoadingDialog();
                    gasActivity.get().showNotificationStatus(
                        gasNotification.getGas().equals("on")
                    );
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    gasActivity.get().hideLoadingDialog();
                    e.printStackTrace();
                    gasActivity.get().showToast("Can't get data from server");
                }

                @Override
                public void onComplete() {

                }
            });
    }

    public void turnGasTrackingOn(){
        Log.d(TAG, "User turn gas tracking on");
        setNotificationStatusApi("on");
    }

    public void turnGasTrackingOff(){
        Log.d(TAG, "User turn gas tracking off");
        setNotificationStatusApi("off");
    }

    private void setNotificationStatusApi(String status){
        notificationService.setGasNotificationStatus(new GasNotification(status))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Void>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {
                    gasActivity.get().showLoadingDialog();
                }

                @Override
                public void onNext(@NonNull Void unused) {
                    gasActivity.get().hideLoadingDialog();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    gasActivity.get().hideLoadingDialog();
                    e.printStackTrace();
                    if (e.getClass() != NullPointerException.class)
                        gasActivity.get().showToast("Fail to update notification settings");
                }

                @Override
                public void onComplete() {

                }
            });
    }

    public MutableLiveData<String> getGasStatus() {
        return gasStatus;
    }

    public void setGasStatus(MutableLiveData<String> gasStatus) {
        this.gasStatus = gasStatus;
    }

    public MutableLiveData<Boolean> getGasSensorOn() {
        return gasSensorOn;
    }

    public void setGasSensorOn(MutableLiveData<Boolean> gasSensorOn) {
        this.gasSensorOn = gasSensorOn;
    }

    public void onBackButtonClicked(){
        gasActivity.get().goBack();
    }

}
