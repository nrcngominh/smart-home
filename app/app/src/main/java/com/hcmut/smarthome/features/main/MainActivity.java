package com.hcmut.smarthome.features.main;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hcmut.smarthome.R;
import com.hcmut.smarthome.features.authentication.SignInActivity;
import com.hcmut.smarthome.features.door.DoorActivity;
import com.hcmut.smarthome.features.gas.GasActivity;
import com.hcmut.smarthome.features.main.settings.SignOutEvent;
import com.hcmut.smarthome.utils.NotificationUtil;
import com.hcmut.smarthome.utils.SharedPreferencesHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//            R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
//            .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        navView.setSelectedItemId(R.id.navigation_home);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NotificationUtil.createNotificationChannel(this,
                NotificationManager.IMPORTANCE_HIGH,
                true,
                "Warning",
                "Warning about dangerous event");
        }
        checkIntentExtras();
    }

    private void checkIntentExtras() {
        if (getIntent().getExtras() != null){
            if (getIntent().getStringExtra("name").equals("GAS")){
                Intent intent = new Intent(this, GasActivity.class);
                intent.putExtra(GasActivity.GAS_WARNING, true);
                startActivity(intent);
            }
            else if (getIntent().getStringExtra("name").equals("MAGNETIC")){
                Intent intent = new Intent(this, DoorActivity.class);
                intent.putExtra(DoorActivity.DOOR_WARNING, true);
                startActivity(intent);
            }
//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.d(TAG, key + ": " +  value);
//            }
        }
    }

    @Override
    public void onBackPressed() {
        if (navView.getSelectedItemId() == R.id.navigation_home){
            Intent intent = new Intent(this, SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(SignInActivity.SHOULD_FINISH, true);
            startActivity(intent);
        }
        else {
            super.onBackPressed();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onSignOutEvent(SignOutEvent event){
        SharedPreferencesHelper.setSignInStatus(this, false);
        finish();
    }

}
