package com.hcmut.smarthome.api.mqtt;

import com.hcmut.smarthome.Config;

public class Topics {
    public static String getLedTopic() {
        return Config.PREFIX1 + "bk-iot-led";
    }

    public static String getBuzzerTopic() {
        return Config.PREFIX1 + "bk-iot-speaker";
    }

    public static String getLcdTopic() {
        return Config.PREFIX1 + "bk-iot-lcd";
    }

    public static String getDht11Topic() {
        return Config.PREFIX1 + "bk-iot-temp-humid";
    }

    public static String getMagneticTopic() {
        return Config.PREFIX1 + "bk-iot-magnetic";
    }

    public static String getGasTopic() {
        return Config.PREFIX2 + "bk-iot-gas";
    }
}
