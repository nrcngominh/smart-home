package com.hcmut.smarthome.features.authentication.events;

public class SignInResponseEvent {
    boolean isSuccessful;

    public SignInResponseEvent(boolean isSuccessful) {
        this.isSuccessful = isSuccessful;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

}
