package com.hcmut.smarthome.services;

public interface AccountService {
    void login(String username, String password, AccountServiceObserver observer);
}
