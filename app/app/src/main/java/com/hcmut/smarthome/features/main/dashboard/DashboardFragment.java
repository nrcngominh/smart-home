package com.hcmut.smarthome.features.main.dashboard;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.api.mqtt.LedMode;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.MqttServiceImpl;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.databinding.FragmentDashboardBinding;
import com.hcmut.smarthome.features.main.dashboard.events.OpenLightDialogEvent;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class DashboardFragment extends Fragment {

    private DashboardViewModel viewModel;
    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MqttService mqttService = new MqttServiceImpl(getContext());
        viewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        viewModel.setupMqttService(mqttService);
        viewModel.getLastLightStatus();
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_dashboard,
            container,
            false
        );
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        Thread mqttThread = new Thread(() -> {
            try {
                mqttService.connect(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        try {
                            mqttService.subscribe(Topics.getDht11Topic(), null);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.d("MQTT", "Connect failed");
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
        mqttThread.start();

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onLoadActivityEvent(LoadActivityEvent event){
        Intent intent = new Intent(getActivity(), event.getaClass());
        startActivity(intent);
    }

    @Subscribe
    public void onOpenLightDialogEvent(OpenLightDialogEvent event){
        String currentStatus = event.getCurrentStatus();
        View lightOption = LayoutInflater.from(getContext()).inflate(
            R.layout.dialog_light_control,
            null
        );
        setCurrentStatus(lightOption, currentStatus);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(lightOption);
        builder.setTitle("Light control panel");
        builder.setNegativeButton("Cancel", null);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RadioGroup radioGroup = lightOption.findViewById(R.id.rdogr_light_control);
                int selectedId = radioGroup.getCheckedRadioButtonId();
                switch (selectedId) {
                    case R.id.rdo_light_off:
                        viewModel.switchLed(LedMode.OFF);
                        break;
                    case R.id.rdo_light_red:
                        viewModel.switchLed(LedMode.RED);
                        break;
                    case R.id.rdo_light_green:
                        viewModel.switchLed(LedMode.BLUE);
                        break;
                }
            }
        });
        builder.show();
    }

    private void setCurrentStatus(View lightOption, String currentStatus) {
        if (currentStatus.equals(LedMode.OFF.getData())){
            RadioButton btn = lightOption.findViewById(R.id.rdo_light_off);
            btn.setChecked(true);
        }
        else if (currentStatus.equals(LedMode.RED.getData())){
            RadioButton btn = lightOption.findViewById(R.id.rdo_light_red);
            btn.setChecked(true);
        }
        else if (currentStatus.equals(LedMode.BLUE.getData())){
            RadioButton btn = lightOption.findViewById(R.id.rdo_light_green);
            btn.setChecked(true);
        }
    }

}
