package com.hcmut.smarthome.api.mqtt;

public interface MqttHandler {
    void handle(Object payload);
}
