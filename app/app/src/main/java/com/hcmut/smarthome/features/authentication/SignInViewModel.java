package com.hcmut.smarthome.features.authentication;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.hcmut.smarthome.features.authentication.events.SignInRequestEvent;
import com.hcmut.smarthome.features.authentication.events.SignInResponseEvent;
import com.hcmut.smarthome.services.AccountService;
import com.hcmut.smarthome.services.AccountServiceObserver;

import org.greenrobot.eventbus.EventBus;

public class SignInViewModel extends ViewModel {
    public static final String TAG = SignInViewModel.class.getSimpleName();
    public AccountService accountService;

    public SignInViewModel(AccountService accountService) {
        this.accountService = accountService;
    }

    public void onSignInClicked() {
        EventBus.getDefault().post(new SignInRequestEvent());
    }

    public void validateUser(String username, String password) {
        Log.d(TAG, "Validating username: " + username + " password: " + password);
        AccountServiceObserver observer = isSuccessful -> EventBus.getDefault().post(new SignInResponseEvent(isSuccessful));
        accountService.login(username, password, observer);
    }
}
