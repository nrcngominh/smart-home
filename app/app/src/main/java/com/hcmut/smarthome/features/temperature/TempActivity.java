package com.hcmut.smarthome.features.temperature;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.MqttServiceImpl;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.databinding.ActivityTempBinding;
import com.hcmut.smarthome.features.draw_chart.DrawChartActivity;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TempActivity extends AppCompatActivity {
    private TempViewModel viewModel;
    private ActivityTempBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MqttService mqttService = new MqttServiceImpl(this);
        viewModel = new TempViewModel(mqttService);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_temp);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        Thread mqttThread = new Thread(() -> {
            try {
                mqttService.connect(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        try {
                            mqttService.subscribe(Topics.getDht11Topic(), null);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.d("MQTT", "Connect failed");
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
        mqttThread.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe()
    public void onPopupMenuClickedEvent(PopupMenuClickedEvent event){
        PopupMenu popupMenu = new PopupMenu(this, event.getClickedView());
        popupMenu.getMenuInflater().inflate(R.menu.temp_humid_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.today_temperature:
                        onTodayTemperatureClicked();
                        break;
                    case R.id.today_humidity:
                        onTodayHumidityClicked();
                        break;
                    case R.id.past_temperature:
                        onPastTemperatureClicked();
                        break;
                    case R.id.past_humidity:
                        onPastHumidityClicked();
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    private void onPastHumidityClicked() {
        loadDrawChartActivity(DrawChartActivity.HUMIDITY, DrawChartActivity.PAST);
    }

    private void onPastTemperatureClicked() {
        loadDrawChartActivity(DrawChartActivity.TEMPERATURE, DrawChartActivity.PAST);
    }

    private void onTodayHumidityClicked() {
        loadDrawChartActivity(DrawChartActivity.HUMIDITY, DrawChartActivity.TODAY);
    }

    private void onTodayTemperatureClicked() {
        loadDrawChartActivity(DrawChartActivity.TEMPERATURE, DrawChartActivity.TODAY);
    }

    private void loadDrawChartActivity(String dataType,String timeType){
        Intent intent = new Intent(this, DrawChartActivity.class);
        // Put extra here
        intent.putExtra(DrawChartActivity.DATA_TYPE, dataType);
        intent.putExtra(DrawChartActivity.TIME_TYPE, timeType);
        startActivity(intent);
    }

    @Subscribe
    public void onGoBackEvent(GoBackEvent event){
        onBackPressed();
    }

}
