package com.hcmut.smarthome.features.gas;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.databinding.ActivityGasBinding;
import com.hcmut.smarthome.features.door.DoorViewModel;

import java.lang.ref.WeakReference;

public class GasActivity extends AppCompatActivity {
    public static final String GAS_WARNING = "gas_warning";
    private GasViewModel viewModel;
    private ActivityGasBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gas);
        viewModel = new ViewModelProvider(this).get(GasViewModel.class);
        viewModel.initialize(new WeakReference<>(this));

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        checkWarningInIntent();
    }
    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    public void showNotificationStatus(boolean status){
        binding.swGasTrackingOn.setChecked(status);
        setSwitchListener();
    }
    public void showLoadingDialog(){
        binding.layoutLoading.setVisibility(View.VISIBLE);
    }
    public void hideLoadingDialog(){
        binding.layoutLoading.setVisibility(View.GONE);
    }
    private void setSwitchListener() {
        binding.swGasTrackingOn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                viewModel.turnGasTrackingOn();
            else
                viewModel.turnGasTrackingOff();
        });
    }

    private void checkWarningInIntent() {
        if (getIntent().getExtras() != null && getIntent().getBooleanExtra(GAS_WARNING, false)){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(Html.fromHtml("<font color='#DC1D1D'>WARNING</font>"));
            builder.setMessage(Html.fromHtml("<font color='#DC1D1D'>Gas is leaking in your house! Please check and confirm it!</font>"));
            builder.setPositiveButton("Confirm", (dialog, which) -> {
                // User confirms that the gas is leaking
                viewModel.getGasStatus().postValue("Leaked");
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> {
                // User dismisses the warning
            });
            builder.show();
        }
    }

    public void goBack(){
        onBackPressed();
    }

}
