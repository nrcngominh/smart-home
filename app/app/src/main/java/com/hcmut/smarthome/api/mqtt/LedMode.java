package com.hcmut.smarthome.api.mqtt;

public enum LedMode {
    OFF("0"),
    RED("1"),
    BLUE("2");

    final String data;

    LedMode(String data) {
        this.data = data;
    }

    public String getData() {
        return this.data;
    }
}
