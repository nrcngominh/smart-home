package com.hcmut.smarthome.features.draw_chart;

import com.hcmut.smarthome.data.TempAndHumid;

import java.util.List;

public class DrawChartEvent {
    private String title;
    private String type;
    private String timeFormat;
    private List<TempAndHumid> dataList;

    public DrawChartEvent(String title, String type, String timeFormat, List<TempAndHumid> dataList) {
        this.title = title;
        this.type = type;
        this.timeFormat = timeFormat;
        this.dataList = dataList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    public List<TempAndHumid> getDataList() {
        return dataList;
    }

    public void setDataList(List<TempAndHumid> dataList) {
        this.dataList = dataList;
    }
}
