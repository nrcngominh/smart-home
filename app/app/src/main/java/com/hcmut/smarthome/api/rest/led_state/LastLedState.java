package com.hcmut.smarthome.api.rest.led_state;


import com.google.gson.Gson;
import com.hcmut.smarthome.api.mqtt.FeedData;



public class LastLedState {

    private String value;
    private FeedData data;
// goi den fuction getLedState de lay du lieu tra ve, kieu du lieu la FeddData
    public FeedData getLedState(){
        if(data==null){
            //todo
            Gson gson = new Gson();
            data=gson.fromJson(this.value,FeedData.class);
        }
        return data;
    }

}
