package com.hcmut.smarthome.features.door;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.databinding.ActivityDoorBinding;

import java.lang.ref.WeakReference;

public class DoorActivity extends AppCompatActivity {

    public static final String DOOR_WARNING = "door_warning";
    private DoorViewModel viewModel;
    private ActivityDoorBinding binding;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_door);

        viewModel = new ViewModelProvider(this).get(DoorViewModel.class);
        viewModel.initialize(new WeakReference<>(this));

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        checkWarningInIntent();
    }

    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void showNotificationStatus(boolean status){
        binding.swDoorTrackingOn.setChecked(status);
        setSwitchListener();
    }

    public void showLoadingDialog(){
        binding.layoutLoading.setVisibility(View.VISIBLE);
    }

    public void hideLoadingDialog(){
        binding.layoutLoading.setVisibility(View.GONE);
    }

    private void setSwitchListener() {
        binding.swDoorTrackingOn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                viewModel.turnDoorTrackingOn();
            else
                viewModel.turnDoorTrackingOff();
        });
    }

    private void checkWarningInIntent() {
        if (getIntent().getExtras() != null && getIntent().getBooleanExtra(DOOR_WARNING, false)){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(Html.fromHtml("<font color='#DC1D1D'>WARNING</font>"));
            builder.setMessage(Html.fromHtml("<font color='#DC1D1D'>Your door is opened illegally! Please check and confirm it!</font>"));
            builder.setPositiveButton("Confirm", (dialog, which) -> {
                // User confirms that the door is opened illegally
                viewModel.getDoorLockStatus().postValue("Opened");
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> {
                // User dismisses the warning
            });
            builder.show();
        }
    }

    public void goBack(){
        onBackPressed();
    }

}
