package com.hcmut.smarthome.api.mqtt;

public class Dht11Data {
    public int getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    final int temperature;
    final int humidity;

    public Dht11Data(int temperature, int humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public static Dht11Data parse(String raw) {
        String[] tokens = raw.split("-");
        int temp = Integer.parseInt(tokens[0]);
        int humid = Integer.parseInt(tokens[1]);
        return new Dht11Data(temp, humid);
    }
}
