package com.hcmut.smarthome.api.rest.notification;

public class GasNotification {
    private String gas;

    public GasNotification(String gas) {
        this.gas = gas;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }
}
