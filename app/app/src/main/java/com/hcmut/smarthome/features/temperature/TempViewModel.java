package com.hcmut.smarthome.features.temperature;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hcmut.smarthome.api.mqtt.Dht11Data;
import com.hcmut.smarthome.api.mqtt.FeedData;
import com.hcmut.smarthome.api.mqtt.FeedDataBuilder;
import com.hcmut.smarthome.api.mqtt.MqttHandler;
import com.hcmut.smarthome.api.mqtt.MqttService;
import com.hcmut.smarthome.api.mqtt.Topics;
import com.hcmut.smarthome.api.rest.data_log.RawLogData;
import com.hcmut.smarthome.data.TempAndHumid;
import com.hcmut.smarthome.features.draw_chart.DrawChartActivity;
import com.hcmut.smarthome.services.DataLogService;
import com.hcmut.smarthome.services.LedService;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class TempViewModel extends ViewModel {
    public static final String TAG = TempViewModel.class.getSimpleName();
    private final Calendar cldr = Calendar.getInstance();
    private DataLogService dataLogService = new DataLogService();
    MutableLiveData<String> currentTemperature = new MutableLiveData<>("loading");
    MutableLiveData<String> currentHumid = new MutableLiveData<>("loading");

    MqttService mqttService;
    FeedDataBuilder builder = new FeedDataBuilder();
    Gson gson = new Gson();

    public TempViewModel(MqttService service) {
        this.mqttService = service;
        mqttService.on(Topics.getDht11Topic(), new MqttHandler() {
            @Override
            public void handle(Object payload) {
                Gson gson = new Gson();
                FeedData data = gson.fromJson((String) payload, FeedData.class);
                Dht11Data dht11Data = Dht11Data.parse(data.getData());
                updateHumid(dht11Data.getHumidity());
                updateTemperature(dht11Data.getTemperature());
            }
        });
        getLastTempHumid();
    }

    public void onMenuClicked(View clickedView){
        EventBus.getDefault().post(new PopupMenuClickedEvent(clickedView));
    }

    public void updateTemperature(double value) {
        Log.d(TAG, "New temperature value received");
        currentTemperature.postValue("" + value + "\u2103");
    }

    public void updateHumid(double value) {
        Log.d(TAG, "New humid value received");
        currentHumid.postValue("" + value + "%");
    }

    public MutableLiveData<String> getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(MutableLiveData<String> currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public MutableLiveData<String> getCurrentHumid() {
        return currentHumid;
    }

    public void setCurrentHumid(MutableLiveData<String> currentHumid) {
        this.currentHumid = currentHumid;
    }

    public void onBackButtonClicked(){
        EventBus.getDefault().post(new GoBackEvent());
    }

    public void getLastTempHumid() {
        long from = getLastNDaysTime(0);
        long to = System.currentTimeMillis() / 1000;
        Log.d("Time", "From: " + from + " To: " + to);
        // Call service here
        callApi(from, to, "Today temperature log", DrawChartActivity.TEMPERATURE, "HH:mm");

    }

    private long getLastNDaysTime(int days) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy", Locale.US);
            int cDay = cldr.get(Calendar.DAY_OF_MONTH);
            int cMonth = cldr.get(Calendar.MONTH);
            int cYear = cldr.get(Calendar.YEAR);
            Date currentDate = simpleDateFormat.parse("" + cDay + "/" + (cMonth + 1) + "/" + cYear);
            return (currentDate.getTime() - days * 24 * 60 * 60 * 1000) / 1000 + 60;
        } catch (ParseException e) {
            Log.d("GetDateTime", "Fail to calculate date time");
            return System.currentTimeMillis() / 1000;
        }
    }

    private void callApi(long from, long to, String title, String type, String timeFormat) {
        dataLogService.getDataLog(from, to)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<RawLogData>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull List<RawLogData> rawLogData) {
                    Log.d("DrawChart", "OnNext");
                    if (rawLogData.isEmpty()){
                        Log.d("DrawChart", "No data");
                    }
                    else {
                        Log.d("DrawChart", "There's " + rawLogData.size());
                        ArrayList<TempAndHumid> parsedData = new ArrayList<>();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        if (rawLogData.size() > 0) {
                            RawLogData latestData = rawLogData.get(rawLogData.size() - 1);
                            updateTemperature(latestData.getTemperature());
                            updateHumid(latestData.getHumidity());
                        }
                    }

                }

                @Override
                public void onError(@NonNull Throwable e) {
                    Log.d("DrawChart", "onError");
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
    }

}
