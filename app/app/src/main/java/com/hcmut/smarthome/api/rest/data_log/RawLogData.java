package com.hcmut.smarthome.api.rest.data_log;

public class RawLogData {
    private double temperature;
    private double humidity;
    private String datetime;

    public RawLogData(double temperature, double humidity, String datetime) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.datetime = datetime;
    }

    public double getTemperature() {
        return temperature;
    }
    public double getHumidity() {
        return humidity;
    }
    public String getDatetime() {
        return datetime;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
