package com.hcmut.smarthome.services;

public interface AccountServiceObserver {
    void raise(boolean isSuccessful);
}
