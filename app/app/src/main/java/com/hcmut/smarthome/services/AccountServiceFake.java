package com.hcmut.smarthome.services;

public class AccountServiceFake implements AccountService {
    @Override
    public void login(String username, String password, AccountServiceObserver observer) {
        observer.raise(true);
    }
}
