package com.hcmut.smarthome.features.temperature;

import android.view.View;

public class PopupMenuClickedEvent {
    private View clickedView;

    public PopupMenuClickedEvent(View clickedView) {
        this.clickedView = clickedView;
    }

    public void setClickedView(View clickedView) {
        this.clickedView = clickedView;
    }

    public View getClickedView() {
        return clickedView;
    }
}
