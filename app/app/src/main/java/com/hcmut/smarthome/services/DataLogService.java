package com.hcmut.smarthome.services;

import com.hcmut.smarthome.api.rest.data_log.DataLogAPI;
import com.hcmut.smarthome.api.rest.data_log.RawLogData;
import com.hcmut.smarthome.api.rest.RetrofitClient;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public class DataLogService {
    public Observable<List<RawLogData>> getDataLog(long from, long to) {
        return RetrofitClient.getInstance()
            .getClient()
            .create(DataLogAPI.class)
            .getDataLogs(from, to);
    }
}
