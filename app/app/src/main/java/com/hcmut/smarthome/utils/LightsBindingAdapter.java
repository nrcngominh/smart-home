package com.hcmut.smarthome.utils;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.BindingAdapter;

import com.hcmut.smarthome.R;
import com.hcmut.smarthome.api.mqtt.LedMode;

public class LightsBindingAdapter {
    @BindingAdapter("app:lightTint")
    public static void setImageTint(ImageView imageView, LedMode mode){
        switch (mode){
            case RED:
                imageView.setImageDrawable(AppCompatResources.getDrawable(
                    imageView.getContext(),
                    R.drawable.ic_light_red
                ));
                break;
            case BLUE:
                imageView.setImageDrawable(AppCompatResources.getDrawable(
                    imageView.getContext(),
                    R.drawable.ic_light_green
                ));
                break;
            case OFF:
                imageView.setImageDrawable(AppCompatResources.getDrawable(
                    imageView.getContext(),
                    R.drawable.ic_light_off
                ));
                break;
        }
    }

    @BindingAdapter("app:lightStatus")
    public static void setLightStatus(TextView textView, LedMode mode){
        switch (mode){
            case RED:
                textView.setText("Red");
                break;
            case BLUE:
                textView.setText("Green");
                break;
            case OFF:
                textView.setText("Off");
                break;
        }
    }
}
