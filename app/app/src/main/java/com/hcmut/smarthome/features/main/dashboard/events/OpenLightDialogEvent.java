package com.hcmut.smarthome.features.main.dashboard.events;

public class OpenLightDialogEvent {
    private String currentStatus;

    public OpenLightDialogEvent(String status){
        currentStatus = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
