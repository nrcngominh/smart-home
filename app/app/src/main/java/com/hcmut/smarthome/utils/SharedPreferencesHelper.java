package com.hcmut.smarthome.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {
    public static final String USER_PREFS = "user_prefs";

    public static final String SIGN_IN_STATUS = "sign_in_status";

    public static boolean getSignInStatus(Context context){
        return context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
            .getBoolean(SIGN_IN_STATUS, false);
    }

    public static void setSignInStatus(Context context, boolean status){
        context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
            .edit()
            .putBoolean(SIGN_IN_STATUS, status)
            .apply();
    }
}
