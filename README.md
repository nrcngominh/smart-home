# Smart Home

## Prerequisites

- `docker` 20.x.x.
- `docker-compose` 1.28.x.
- `Android Studio` (only for app).

## Setup

### API server and database

Make sure you have started `docker` service.

Run script to start containers:

```
scripts/start.sh
```

Test if server is working:

```bash
curl --location --request POST 'localhost/api/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "abcdef",
    "password": "123456"
}'
```

### Android

Copy gradle properties file:

```
cd app/app
cp gradle.properties.example gradle.properties
```

Modify `gradle.properties` for your environment:

- Set `SERVER_HOST` to your local hostname or local IP address.

### Firebase

Copy Firebase Admin private key file to `server/serviceAccountKey.json`

## Dashboard

Visit `localhost/dashboard` to interact with simulated devices.
