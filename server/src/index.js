const app = require("./app.js")();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const pgClient = require("./services/postgres-client");
const mqttClient = require("./services/mqtt-client");
const feedData = require("./repositories/feedData");
const removeData = require("./repositories/removeData");
const axios = require("axios");

const main = async () => {
  try {
    // Connect to PostgreSQL
    await pgClient.connect();
    console.log("Connected to PostgreSQL");

    // Setup Socket.IO
    io.on("connection", (socket) => {
      console.log("A client connected!");
    });

    // Start HTTP server
    server.listen(process.env.PORT || 3000, () => {
      console.log("Server is running on port", server.address().port);
    });

    //Download all data from adafruit temp-humid
    async function getDataFromAdafruit() {
      try {
        const response = await axios.get(
          "https://io.adafruit.com/api/v2/CSE_BBC/feeds/bk-iot-temp-humid/data"
        );

        // truncate data
        await removeData.removeTempTable();
        await removeData.removeHumidTable();

        var res = response.data.reverse();
        console.log("index.js Download all data from adafruit temp-humid");
        res.forEach(async (element) => {
          var temp_humid = JSON.parse(element.value).data;

          await feedData.create(
            element.id,
            " ",
            temp_humid,
            " ",
            element.created_epoch
          );
        });
        console.log("Da insert");
      } catch (error) {
        console.error(error);
      }
    }

    getDataFromAdafruit();

    // Connect to Broker
    mqttClient.connect();
  } catch (error) {
    console.error(error);
    console.trace();
  }
};

main();
