const TOPICS = require("./topic");

const speak = (client, volume) => {
  // TODO: Nhon
  // Use MQTT client to set speaker to max volume (1023)
  const topic = TOPICS.SPEAKER;
  const data = {
    id: "3",
    name: "SPEAKER",
    data: volume,
    unit: "",
  };
  client.publish(topic, JSON.stringify(data));
  console.log("SEND: ", { topic, data });
};

module.exports = {
  speak,
};
