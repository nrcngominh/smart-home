const mqtt = require("mqtt");
const { sendNotification } = require("../services/fcm");
const speaker = require("./speaker");
const TOPICS = require("./topic");
const feedData = require("../repositories/feedData");

// Connect to broker
const url = `mqtt://${process.env.BROKER_HOST1}`;

// Connect to broker
const url2 = `mqtt://${process.env.BROKER_HOST2}`;

const connect = () => {
  const client = mqtt.connect(url);
  const client2 = mqtt.connect(url2);

  client.on("connect", () => {
    console.log("Client 1 connected");
    client.subscribe(TOPICS.MAGNETIC, function (err) {
      if (err) {
        console.error(err);
      }
    });

    client.subscribe(TOPICS.LED, function (err) {
      if (err) {
        console.error(err);
      }
    });

    client.subscribe(TOPICS.DHT11, function (err) {
      if (err) {
        console.error(err);
      }
    });

    client.subscribe(TOPICS.SPEAKER, function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client2.on("connect", () => {
    console.log("Client 2 connected");

    client2.subscribe(TOPICS.GAS, function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on("message", async (topicByte, messageByte) => {
    const topic = topicByte.toString();
    const wrapper = JSON.parse(messageByte.toString());
    console.log("RECV: ", { topic, wrapper });

    // Store feed data to database
    // TODO: Phuc

    feedData.create(
      wrapper.id,
      wrapper.name,
      wrapper.data,
      wrapper.unit,
      Date.now() / 1000
    );

    // Handle message
    if (topic === TOPICS.MAGNETIC && checkDisableDoor == false) {
      const OPEN = "1";
      if (wrapper.data === OPEN) {
        try {
          const res = await sendNotification(
            "WARNING",
            "Door was opened",
            wrapper
          );
          console.log(res);

          // Max buzzer
          speaker.speak(client, "1023");
        } catch (err) {
          console.error(err);
        }
      }
    }
  });

  client2.on("message", async (topicByte, messageByte) => {
    console.log("Client 2 receive message");
    const topic = topicByte.toString();
    const wrapper = JSON.parse(messageByte.toString());
    console.log("RECV: ", { topic, wrapper });

    // Store feed data to database
    // TODO: Phuc

    feedData.create(
      wrapper.id,
      wrapper.name,
      wrapper.data,
      wrapper.unit,
      Date.now() / 1000
    );

    if (topic === TOPICS.GAS && checkDisableGas == false) {
      const LEAK = "1";
      if (wrapper.data === LEAK) {
        speaker.speak(client2, "1023");
        try {
          const res = await sendNotification(
            "WARNING",
            "Gas is leaking",
            wrapper
          );
          console.log(res);

          // Max buzzer
        } catch (err) {
          console.error(err);
        }
      }
    }
  });
};

module.exports = {
  connect,
};
