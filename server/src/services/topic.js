module.exports = {
  LED: process.env.PREFIX1 + "bk-iot-led",
  MAGNETIC: process.env.PREFIX1 + "bk-iot-magnetic",
  DHT11: process.env.PREFIX1 + "bk-iot-temp-humid",
  SPEAKER: process.env.PREFIX1 + "bk-iot-speaker",
  GAS: process.env.PREFIX2 + "bk-iot-gas",
};
