const { createClient } = require("redis");
const { promisify } = require("util");

const client = createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

console.log("Connected to Redis");

const get = promisify(client.get).bind(client);
const set = promisify(client.set).bind(client);

module.exports = {
  get,
  set,
};
