const admin = require("firebase-admin");
const TokenRepo = require("../repositories/registrationToken");

admin.initializeApp({
  credential: admin.credential.cert(require("../../serviceAccountKey.json")),
});

const sendNotification = async (title, body, data) => {
  console.log("Send notification");

  const token = await TokenRepo.retrieve();
  if (!token) {
    return Promise.reject("No app registered");
  }
  const message = {
    notification: {
      title,
      body,
    },
    data,
    token,
  };

  return admin.messaging().send(message);
};

module.exports = {
  sendNotification,
};
