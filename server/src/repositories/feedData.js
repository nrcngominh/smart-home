const client = require("../services/postgres-client");

// convert epoch time
var getDate = (epochTime) => {
  var date = new Date(0);
  date.setUTCSeconds(epochTime);
  var formatDate =
    date.getUTCFullYear() +
    "-" +
    0 +
    (date.getUTCMonth() + 1) +
    "-" +
    date.getUTCDate() +
    " " +
    date.getUTCHours() +
    ":" +
    date.getUTCMinutes() +
    ":" +
    date.getUTCSeconds();
  return formatDate;
};

// insert temp-humid to db
const create = async (id, name, data, unit, datetime) => {
  //TODO: Phong;
  try {
    var arr = data.split("-");
    var temperature = arr[0];
    var humidity = arr[1];
    var dateTimeHuman = getDate(datetime);
    await insertHumid(humidity, 1, dateTimeHuman);
    await insertTemp(temperature, 1, dateTimeHuman);
  } catch (err) {
    console.log(err.stack);
  }
};

// humid
const insertHumid = async (humidity, roomid, datetime) => {
  var sql = `INSERT INTO humid_records(humidity, roomid, datetime) VALUES(${humidity},${roomid},'${datetime}')`;
  try {
    const res = await client.query(sql);
  } catch (err) {
    console.log(err.stack);
  }
};

const insertTemp = async (temperature, roomid, datetime) => {
  var sql = `INSERT INTO temp_records(temperature, roomid, datetime) VALUES(${temperature},${roomid},'${datetime}')`;
  try {
    const res = await client.query(sql);
  } catch (err) {
    console.log(err.stack);
  }
};

const insertAction = (actionname, roomid, id, datetime) => {
  var sql = `INSERT INTO action(actionname, roomid, id, datetime)
	VALUES
	('${actionname}',${roomid},${id},'${datetime}')`;
  try {
    const res = client.query(sql);
    console.log("1 record inserted");
  } catch (err) {
    console.log(err.stack);
  }
};

const insertGasLog = (aid, datetime) => {
  var sql = `INSERT INTO gaslog(aid, datetime)	VALUES(${aid},'${datetime}')`;
  try {
    const res = client.query(sql);
    console.log("1 record inserted");
  } catch (err) {
    console.log(err.stack);
  }
};

const insertDoorLog = (aid, datetime) => {
  var sql = `INSERT INTO doorlog(aid, datetime)	VALUES(${aid},'${datetime}')`;
  try {
    const res = client.query(sql);
    console.log("1 record inserted");
  } catch (err) {
    console.log(err.stack);
  }
};

const insertLedLog = (aid, datetime) => {
  var sql = `INSERT INTO controlled(aid, datetime)	VALUES(${aid},'${datetime}')`;
  try {
    const res = client.query(sql);
    console.log("1 record inserted");
  } catch (err) {
    console.log(err.stack);
  }
};

module.exports = {
  insertHumid,
  insertTemp,
  insertAction,
  insertDoorLog,
  insertGasLog,
  insertLedLog,
  create,
};
