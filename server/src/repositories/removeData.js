const client = require("../services/postgres-client");

// truncate humid
const removeHumidTable = async () => {
  var sql = `TRUNCATE TABLE humid_records;`;
  try {
    const res = await client.query(sql);
  } catch (err) {
    console.log(err.stack);
  }
};

// truncate temp
const removeTempTable = async () => {
  var sql = `TRUNCATE TABLE temp_records;`;
  try {
    const res = await client.query(sql);
  } catch (err) {
    console.log(err.stack);
  }
};

module.exports = { removeHumidTable, removeTempTable };
