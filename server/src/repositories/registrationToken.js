const client = require("../services/redis-client");

const REGISTRATION_TOKEN = "REGISTRATION_TOKEN";

const register = (token) => {
  return client.set(REGISTRATION_TOKEN, token);
};

const retrieve = () => {
  return client.get(REGISTRATION_TOKEN);
};

module.exports = {
  register,
  retrieve,
};
