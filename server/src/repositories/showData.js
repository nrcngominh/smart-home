const client = require("../services/postgres-client");

//show humid by roomid
const showHumidByRoomid = (roomid) => {
  var sql = `select H.humidid, H.humidity , R.roomname , H.datetime
	from
	humid_records as H inner join rooms as R
	on H.roomid =${roomid} and H.roomid = R.roomid`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showTempByRoomid = (roomid) => {
  var sql = `select T.tempid, T.temperature , R.roomname , T.datetime
  from
  temp_records as T inner join rooms as R
  on T.roomid =${roomid} and T.roomid = R.roomid`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

// const showTempByRoomid2 = async (roomid) => {
//   var sql = `select T.tempid, T.temperature as Nhiet_do, R.roomname as Phong, T.datetime as thoi_gian
//   from
//   temp_records as T inner join rooms as R
//   on T.roomid =${roomid} and T.roomid = R.roomid`;
//   //return client.query(sql);
// };

const showTempHumidByRoomid = (roomid) => {
  var sql = `select T.tempid,H.humidid, T.temperature as Nhiet_do,H.humidity as Do_am, R.roomname as Phong, T.datetime as thoi_gian
	from (humid_records as H
	inner join temp_records as T
	on H.roomid = T.roomid), rooms as R
	where T.datetime = H.datetime and R.roomid = ${roomid} and R.roomid = H.roomid`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showTempHumidByDateTime = async (startDate, endDate) => {
  var sql = `SELECT T.temperature ,H.humidity , T.datetime
  FROM (humid_records as H
  INNER JOIN temp_records as T
  ON H.roomid = T.roomid)
  WHERE T.datetime = H.datetime and T.roomid = 1 and T.roomid = H.roomid
  AND (T.datetime BETWEEN '${startDate}' AND '${endDate}')`;

  try {
    const res = await client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showAction = () => {
  var sql = `select a.aid as STT, a.actionname as Ten_hoat_dong,
	r.roomname as Phong, ac.username as Nguoi_dung,a.datetime as Date_time
from action as a, rooms as r, accounts as ac
where a.roomid = r.roomid and a.id = ac.id`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

// gaslog
const showGasLog = () => {
  var sql = `select a.aid as STT, a.actionname as Ten_hoat_dong,
	r.roomname as Phong, ac.username as Nguoi_dung,a.datetime as Date_time
from gaslog as g, action as a, rooms as r, accounts as ac
where a.aid = g.aid and a.roomid = r.roomid and a.id = ac.id`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showDoorLog = () => {
  var sql = `select a.aid as STT, a.actionname as Ten_hoat_dong,
	r.roomname as Phong, ac.username as Nguoi_dung,a.datetime as Date_time
from doorlog as d, action as a, rooms as r, accounts as ac
where d.aid = a.aid and a.roomid = r.roomid and a.id = ac.id`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showControlled = () => {
  var sql = `select a.aid as STT, a.actionname as Ten_hoat_dong,
	r.roomname as Phong,l.ledid as Den_led , ac.username as Nguoi_dung,
	a.datetime as Date_time
from controlled as c, action as a, rooms as r, accounts as ac, leds as l
where a.aid = c.aid and a.roomid = r.roomid and a.id = ac.id and l.roomid = r.roomid`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showAccounts = () => {
  var sql = `select *	from accounts as ac`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

const showled = () => {
  var sql = `select ledid, l.roomid, roomname
	from leds as l, rooms  as r
	where l.roomid = r.roomid`;

  try {
    const res = client.query(sql);
    console.log("Successful query");
    return res;
  } catch (err) {
    console.log(err.stack);
  }
};

module.exports = {
  showAccounts,
  showAction,
  showControlled,
  showDoorLog,
  showGasLog,
  showHumidByRoomid,
  showTempByRoomid,
  showTempHumidByRoomid,
  showled,
  showTempHumidByDateTime,
};
