const { Router } = require("express");
const TokenRepo = require("../repositories/registrationToken");
const router = Router();

router.post("/", async (req, res) => {
  const { token } = req.body;
  console.log("before register");
  await TokenRepo.register(token);
  console.log("after register");
  res.status(200).send({
    msg: "Received",
  });
});

module.exports = router;
