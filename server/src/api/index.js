const { Router } = require("express");

const router = Router();

router.use("/login", require("./login"));
router.use("/registration-token", require("./registrationToken"));
//api query database
router.use("/temp", require("./temp"));
router.use("/humid", require("./humid"));
router.use("/temp-humid", require("./temp-humid"));

// REST API disable notification
router.use("/gas-notification", require("./gas-notification"));

router.use("/door-notification", require("./door-notification"));

module.exports = router;
