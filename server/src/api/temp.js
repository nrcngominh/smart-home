const { showTempByRoomid } = require("../repositories/showData");
const { Router } = require("express");
const { findByUsername } = require("../repositories/account");
const router = Router();

//get temp record
router.get("/", async function (req, res) {
  try {
    const result = await showTempByRoomid(1);
    res.json(result.rows);
  } catch (err) {
    res.status(500).send({
      msg: "Not available",
    });
    console.error(error);
    console.trace();
  }
});

module.exports = router;
