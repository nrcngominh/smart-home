const { Router } = require("express");
const router = Router();
global.checkDisableDoor = false;

router.post("/", (req, res) => {
  try {
    if (req.body.door == "off") {
      checkDisableDoor = true;
      console.log("Turn off door notifications: " + checkDisableDoor);
      res.status(200).send("Turn off door notifications ");
    } else if (req.body.door == "on") {
      checkDisableDoor = false;
      console.log("Turn on door notifications: " + checkDisableDoor);
      res.status(200).send("Turn on door notifications ");
    } else {
      res.status(400).send({ msg: "Invalid request" });
    }
  } catch (error) {
    // Server error
    res.status(500).send({
      msg: "Not available",
    });
    console.error(error);
    console.trace();
  }
});

router.get("/", (req, res) => {
  console.log("API door get disable notification");
  if (checkDisableDoor == true) {
    res.status(200).send({
      door: "off",
    });
  } else {
    res.status(200).send({
      door: "on",
    });
  }
});

module.exports = router;
