const { Router } = require("express");
const { showHumidByRoomid } = require("../repositories/showData");
const router = Router();

//get humid record
router.get("/", async function (req, res) {
  try {
    const result = await showHumidByRoomid(1);
    res.json(result.rows);
    console.log("quey is running");
    console.log(req.query);
  } catch (err) {
    res.status(500).send({
      msg: "Not available",
    });
    console.error(error);
    console.trace();
  }
});

module.exports = router;
