const { Router } = require("express");
const router = Router();
global.checkDisableGas = false;

router.post("/", (req, res) => {
  try {
    if (req.body.gas == "off") {
      checkDisableGas = true;
      console.log("Turn off gas notifications: " + checkDisableGas);
      res.status(200).send("Turn off gas notifications ");
    } else if (req.body.gas == "on") {
      checkDisableGas = false;
      console.log("Turn on gas notifications: " + checkDisableGas);
      res.status(200).send("Turn on gas notifications ");
    } else {
      res.status(400).send({ msg: "Invalid request" });
    }
  } catch (error) {
    // Server error
    res.status(500).send({
      msg: "Not available",
    });
    console.error(error);
    console.trace();
  }
});

router.get("/", (req, res) => {
  console.log("API gas get disable notification");
  if (checkDisableGas == true) {
    res.status(200).send({
      gas: "off",
    });
  } else {
    res.status(200).send({
      gas: "on",
    });
  }
});

module.exports = router;
