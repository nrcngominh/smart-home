const { findByUsername } = require("../repositories/account");
const { Router } = require("express");

const router = Router();

router.post("/", async (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) {
    // Client error
    res.status(400).send({
      msg: "Invalid username or password",
    });
  } else {
    try {
      const result = await findByUsername(username);
      const account = result.rows[0];
      if (!account || account.password !== password) {
        // Login failed
        res.status(401).send({
          msg: "Login failed",
        });
      } else {
        // Login successful
        res.status(200).send({
          msg: "Login successful",
        });
      }
    } catch (error) {
      // Server error
      res.status(500).send({
        msg: "Not available",
      });
      console.error(error);
      console.trace();
    }
  }
});

module.exports = router;
